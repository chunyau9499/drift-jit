import { WebsocketClient, WS_KEY_MAP, LinearClient, NewLinearOrder, CategoryV5 } from 'bybit-api';

import { logger } from '../common/logger';
import { Order } from '../common/order';
import { Side } from '../common/side';
import { TimeInForce } from '../common/timeInForce';

export class BybitClient {
  public callbackMap: Map<string, Function> = new Map();

  private key: string;
  private secret: string;
  private productType: CategoryV5;
  private ws: WebsocketClient;
  private linearClient: LinearClient;

  private subscriptionChannel: Array<string> = [];

  constructor(key: string, secret: string, productType: CategoryV5, callbackMap: Map<string, Function>) {
    this.key = key;
    this.secret = secret;
    this.productType = productType;
    this.callbackMap = callbackMap;

    this.ws = new WebsocketClient({
      key: this.key,
      secret: this.secret,
      market: 'v5',
    });
    this.linearClient = new LinearClient({
      key: this.key,
      secret: this.secret,
      strict_param_validation: true,
    });
  }
  public subscribeTicker(symbol: string): this {
    this.ws.subscribeV5('tickers.' + symbol, this.productType);
    return this;
  }
  public subscribeOrderBooks(symbols: Array<string>): this {
    this.ws.subscribeV5('orderbook.50.' + symbols, this.productType);
    return this;
  }
  public subscribeOrderBook(symbol: string): this {
    this.ws.subscribeV5('orderbook.50.' + symbol, this.productType);
    return this;
  }
  public subscribePosition(): this {
    this.ws.subscribeV5('position', this.productType);
    return this;
  }
  public subscribePrivateTrades(): this {
    this.ws.subscribeV5('execution', this.productType);
    return this;
  }
  public async init() {
    this.subscriptionChannel.map((channel) => this.ws.subscribe(channel));
  }

  public async run() {
    setTimeout(() => {
      const activePrivateTopics = this.ws.getWsStore().getTopics(WS_KEY_MAP.v5Private);

      logger.info('Active private v5 topics: ', activePrivateTopics);

      const activePublicLinearTopics = this.ws.getWsStore().getTopics(WS_KEY_MAP.v5LinearPublic);
      logger.info('Active public linear v5 topics: ', activePublicLinearTopics);

      const activePublicSpotTopis = this.ws.getWsStore().getTopics(WS_KEY_MAP.v5SpotPublic);
      logger.info('Active public spot v5 topics: ', activePublicSpotTopis);

      const activePublicOptionsTopics = this.ws.getWsStore().getTopics(WS_KEY_MAP.v5OptionPublic);
      logger.info('Active public option v5 topics: ', activePublicOptionsTopics);
    }, 5 * 1000);

    this.ws.on('update', (data) => {
      const strData = JSON.stringify(data);
      this.onUpdate(data);
    });

    this.ws.on('open', (data) => {
      logger.info('connection opened open:', data.wsKey);
    });
    this.ws.on('response', (data) => {
      logger.info('log response: ', JSON.stringify(data, null, 2));
    });
    this.ws.on('reconnect', ({ wsKey }) => {
      logger.info('ws automatically reconnecting.... ', wsKey);
    });
    this.ws.on('reconnected', (data) => {
      logger.info('ws has reconnected ', data?.wsKey);
    });
    this.ws.on('error', (data) => {
      logger.info('ERROR ', data, data?.wsKey);
    });
  }

  public async onUpdate(data: JSON) {
    const topic = data['topic'];
    const topicName = topic.split('.')[0];
    const callback = this.callbackMap.get(topicName);
    if (callback) {
      callback(data);
    } else {
      logger.error(`No callback for topic: ${topicName} ${JSON.stringify(data)}`);
    }
  }

  public async getPositions() {
    return await this.linearClient.getPosition();
  }
  public async sendOrder(order: Order) {
    const bybitSide = order.side === Side.BUY ? 'Buy' : 'Sell';
    const timeInForce = order.timeInForce === 'GoodTillCancel' ? 'GoodTillCancel' : 'ImmediateOrCancel';
    const linearOrder: NewLinearOrder = {
      side: bybitSide,
      symbol: order.symbol,
      order_type: 'Limit',
      qty: order.qty,
      price: order.price,
      time_in_force: timeInForce,
      reduce_only: order.isReduceOnly,
      close_on_trigger: order.isCloseOnTrigger,
      position_idx: 0,
    };
    return await this.linearClient.placeActiveOrder(linearOrder);
  }
}
