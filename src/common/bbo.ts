export class BBO {
  public bestBidPrice = 0;
  public bestBidQty = 0;
  public bestAskPrice = 0;
  public bestAskQty = 0;
  public symbol = '';

  constructor() {}

  public onUpdate(snapshot: Map<string, any>) {
    const data = snapshot['data'];
    this.bestBidPrice = Number(data.bid1Price);
    this.bestBidQty = Number(data.bid1Size);
    this.bestAskPrice = Number(data.ask1Price);
    this.bestAskQty = Number(data.ask1Size);
    this.symbol = data.symbol;
  }
  public getSpread(): number {
    return this.bestAskPrice - this.bestBidPrice;
  }

  public getMidPrice(): number {
    return (this.bestAskPrice + this.bestBidPrice) / 2;
  }

  public isReady(): boolean {
    return this.bestAskPrice > 0 && this.bestBidPrice > 0;
  }

  public setBid(bidPrice: number) {
    this.bestBidPrice = bidPrice;
  }

  public setAsk(askPrice: number) {
    this.bestAskPrice = askPrice;
  }
}
