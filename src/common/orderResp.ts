import { OrderSide, LinearOrderType, LinearTimeInForce } from 'bybit-api';
import { Side } from './side';
import { TimeInForce } from './timeInForce';
import { Order } from './order';

export class OrderResp {
  public order: Order;

  constructor(order: Order) {
    this.order = order;
  }
}
