export function isOrderPriceMoreAggressiveThanRef(orderPrice: number, refPrice: number, isBuy: boolean): boolean {
  if (isBuy) {
    return orderPrice > refPrice;
  }
  return orderPrice < refPrice;
}
export function isOrderPriceMorePassiveThanRef(orderPrice: number, refPrice: number, isBuy: boolean): boolean {
  if (isBuy) {
    return orderPrice < refPrice;
  }
  return orderPrice > refPrice;
}
