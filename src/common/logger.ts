import { createLogger, transports, format } from 'winston';

export const logger = createLogger({
  transports: [new transports.Console(), new transports.File({ filename: 'logs/sample.log' })],
  format: format.combine(
    format.colorize(),
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
    format.printf(({ timestamp, level, message }) => {
      return `[${timestamp}] ${level}: ${message}`;
    })
  ),
});

export const setLogLevel = (logLevel: string) => {
  logger.level = logLevel;
};
