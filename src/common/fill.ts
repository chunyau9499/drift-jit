import { Side } from './side';

export class Fill {
  public side: Side;
  public price: number;
  public qty: number;
  public time: number;
  public orderId: string;
  public symbol: string;

  constructor(side: Side, price: number, qty: number, time: number, orderId: string, symbol: string) {
    this.side = side;
    this.price = price;
    this.qty = qty;
    this.time = time;
    this.orderId = orderId;
    this.symbol = symbol;
  }
}
