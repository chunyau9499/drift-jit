import { logger } from './logger';

export class SpreadPosition {
  public cexDelta: number;
  public dexDelta: number;
  public minDelta: number;

  constructor() {
    this.cexDelta = 0;
    this.dexDelta = 0;
    this.minDelta = 0;
  }
  public getNetDelta() {
    return this.cexDelta + this.dexDelta;
  }
  public isRoughlyBalanced(): boolean {
    return Math.abs(this.getNetDelta()) < this.minDelta;
  }
  public onDeltaUpdate(delta: number, isCex: boolean) {
    if (isCex) {
      this.cexDelta += delta;
    } else {
      this.dexDelta += delta;
    }
  }
  public printDelta() {
    logger.info(`CEX Delta: ${this.cexDelta} DEX Delta: ${this.dexDelta} Net Delta: ${this.getNetDelta()}`);
  }
}
