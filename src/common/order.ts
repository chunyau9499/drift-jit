import { OrderSide, LinearOrderType, LinearTimeInForce } from 'bybit-api';
import { Side } from './side';
import { TimeInForce } from './timeInForce';

export class Order {
  public symbol: string;
  public side: Side;
  public qty: number;
  public price: number;
  public timeInForce: TimeInForce;
  public isReduceOnly = false;
  public isCloseOnTrigger = false;

  constructor(symbol: string, side: Side, qty: number, price: number, timeInForce: TimeInForce) {
    this.symbol = symbol;
    this.side = side;
    this.qty = qty;
    this.price = price;
    this.timeInForce = timeInForce;
  }
}
