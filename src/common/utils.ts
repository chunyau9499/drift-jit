import { Wallet } from '@drift-labs/sdk';
import { Token, TOKEN_PROGRAM_ID, ASSOCIATED_TOKEN_PROGRAM_ID } from '@solana/spl-token';
import { Connection, PublicKey, Transaction, Keypair } from '@solana/web3.js';

import { bs58 } from '@project-serum/anchor/dist/cjs/utils/bytes';
import fs from 'fs';

import { logger } from './logger';
// devnet only
export const TOKEN_FAUCET_PROGRAM_ID = new PublicKey('V4v1mQiAdLz4qwckEb45WqHYceYizoib39cDBHSWfaB');

export async function getOrCreateAssociatedTokenAccount(
  connection: Connection,
  mint: PublicKey,
  wallet: Wallet
): Promise<PublicKey> {
  const associatedTokenAccount = await Token.getAssociatedTokenAddress(
    ASSOCIATED_TOKEN_PROGRAM_ID,
    TOKEN_PROGRAM_ID,
    mint,
    wallet.publicKey
  );

  const accountInfo = await connection.getAccountInfo(associatedTokenAccount);
  if (accountInfo == null) {
    const tx = new Transaction().add(
      Token.createAssociatedTokenAccountInstruction(
        ASSOCIATED_TOKEN_PROGRAM_ID,
        TOKEN_PROGRAM_ID,
        mint,
        associatedTokenAccount,
        wallet.publicKey,
        wallet.publicKey
      )
    );
    const txSig = await connection.sendTransaction(tx, [wallet.payer]);
    const latestBlock = await connection.getLatestBlockhash();
    await connection.confirmTransaction(
      {
        signature: txSig,
        blockhash: latestBlock.blockhash,
        lastValidBlockHeight: latestBlock.lastValidBlockHeight,
      },
      'confirmed'
    );
  }

  return associatedTokenAccount;
}

export function loadCommaDelimitToArray(str: string): number[] {
  try {
    return str
      .split(',')
      .filter((element) => {
        if (element.trim() === '') {
          return false;
        }

        return !isNaN(Number(element));
      })
      .map((element) => {
        return Number(element);
      });
  } catch (e) {
    return [];
  }
}

export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function getWallet(config): Wallet {
  const privateKey = config.global.keeperPrivateKey;
  if (!privateKey) {
    throw new Error(
      'Must set environment variable KEEPER_PRIVATE_KEY with the path to a id.json or a list of commma separated numbers'
    );
  }
  // try to load privateKey as a filepath
  let loadedKey: Uint8Array;
  if (fs.existsSync(privateKey)) {
    logger.info(`loading private key from ${privateKey}`);
    loadedKey = new Uint8Array(JSON.parse(fs.readFileSync(privateKey).toString()));
  } else {
    if (privateKey.includes(',')) {
      logger.info(`Trying to load private key as comma separated numbers`);
      loadedKey = Uint8Array.from(privateKey.split(',').map((val) => Number(val)));
    } else {
      logger.info(`Trying to load private key as base58 string`);
      loadedKey = new Uint8Array(bs58.decode(privateKey));
    }
  }

  const keypair = Keypair.fromSecretKey(Uint8Array.from(loadedKey));
  return new Wallet(keypair);
}

export const getTokenAddress = (mintAddress: string, userPubKey: string): Promise<PublicKey> => {
  return Token.getAssociatedTokenAddress(
    new PublicKey(`ATokenGPvbdGVxr1b2hvZbsiqW5xWH25efTNsLJA8knL`),
    TOKEN_PROGRAM_ID,
    new PublicKey(mintAddress),
    new PublicKey(userPubKey)
  );
};
