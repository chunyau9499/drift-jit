import { AnchorProvider } from '@project-serum/anchor';
import { Connection, Keypair, PublicKey } from '@solana/web3.js';
import {
  DriftClient,
  User,
  initialize,
  Wallet,
  PerpMarkets,
  BulkAccountLoader,
  getMarketsAndOraclesForSubscription,
  SlotSubscriber,
  LogProviderConfig,
  EventSubscriber,
} from '@drift-labs/sdk';
import { Mutex } from 'async-mutex';

import { JITBot } from './jitBot';
import { bs58 } from '@project-serum/anchor/dist/cjs/utils/bytes';

import { logger } from './common/logger';
import { BybitClient } from './core/BybitClient';

const main = async () => {
  const env = 'mainnet-beta';
  // Initialize Drift SDK
  const sdkConfig = initialize({ env });
  require('dotenv').config();

  // Set up the Wallet and Provider
  const privateKey = process.env.SOLANA_PRIVATE_KEY || undefined; // stored as an array string
  const rpcAddress = process.env.RPC_ADDRESS || undefined; // can use: https://api.devnet.solana.com for devnet; https://api.mainnet-beta.solana.com for mainnet;
  const wsEndpoint = process.env.WS_ADDRESS || undefined; // can use: wss://api.devnet.solana.com for devnet; wss://api.mainnet-beta.solana.com for mainnet;

  if (!privateKey) {
    throw new Error('No private key provided');
  }
  if (!rpcAddress) {
    throw new Error('No RPC address provided');
  }
  if (!wsEndpoint) {
    throw new Error('No RPC address provided');
  }

  const keypair = Keypair.fromSecretKey(bs58.decode(privateKey));
  const wallet = new Wallet(keypair);

  // Set up the Connection
  const connection = new Connection(rpcAddress, {
    wsEndpoint: wsEndpoint,
    commitment: 'confirmed',
  });

  // Set up the Provider
  const provider = new AnchorProvider(connection, wallet, AnchorProvider.defaultOptions());

  // Check SOL Balance
  const lamportsBalance = await connection.getBalance(wallet.publicKey);
  logger.info('SOL balance:', lamportsBalance / 10 ** 9);

  const driftPublicKey = new PublicKey(sdkConfig.DRIFT_PROGRAM_ID);
  const bulkAccountLoader = new BulkAccountLoader(connection, 'confirmed', 1000);
  const driftClient = new DriftClient({
    connection,
    wallet: provider.wallet,
    programID: driftPublicKey,
    ...getMarketsAndOraclesForSubscription(env),
    accountSubscription: {
      type: 'polling',
      accountLoader: bulkAccountLoader,
    },
  });
  await driftClient.subscribe();

  // Set up user client
  const user = new User({
    driftClient: driftClient,
    userAccountPublicKey: await driftClient.getUserAccountPublicKey(),
    accountSubscription: {
      type: 'polling',
      accountLoader: bulkAccountLoader,
    },
  });
  const userAccountExists = await user.exists();
  if (!userAccountExists) {
    logger.info('User not exist');
    return;
  }
  const marketInfo = PerpMarkets[env].find((market) => market.baseAssetSymbol === 'SOL');
  const slotSubscriber = new SlotSubscriber(connection);
  await slotSubscriber.subscribe();

  const logProviderConfig: LogProviderConfig = {
    type: 'websocket',
  };
  const eventSubscriber = new EventSubscriber(connection, driftClient.program, {
    maxTx: 8192,
    maxEventsPerType: 8192,
    orderBy: 'blockchain',
    orderDir: 'desc',
    commitment: 'confirmed',
    logProviderConfig,
  });
  const exchKey = process.env.BYBIT_KEY;
  const exchSecret = process.env.BYBIT_SECRET;

  const bot = new JITBot(driftClient, slotSubscriber, marketInfo);

  eventSubscriber.subscribe();
  eventSubscriber.eventEmitter.on('newEvent', async (event) => {
    bot.onEventUpdate(event);
  });
  slotSubscriber.eventEmitter.on('newSlot', async (slot: number) => {
    await bot.onSlotUpdate(slot);
  });

  const callbackMap = new Map();

  callbackMap.set('position', bot.onPosition.bind(bot));
  callbackMap.set('orderbook', bot.onOrderBook.bind(bot));
  callbackMap.set('position', bot.onPosition.bind(bot));
  callbackMap.set('tickers', bot.onBestQuote.bind(bot));
  callbackMap.set('execution', bot.onFill.bind(bot));
  const bybitClient = new BybitClient(exchKey, exchSecret, 'linear', callbackMap);

  const exchSymbol = 'SOLUSDT';

  bybitClient.subscribePosition().subscribePrivateTrades().subscribeTicker(exchSymbol);
  await bybitClient.run();

  bot.setExchClient(bybitClient);
  bot.setExchSymbol(exchSymbol);
  await bot.init();
  await bot.start();
};

main();
