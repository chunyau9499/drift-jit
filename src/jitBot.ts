import {
  DLOB,
  DLOBNode,
  DriftClient,
  EventMap,
  MarketType,
  NodeToFill,
  OrderType,
  PRICE_PRECISION,
  PerpMarketConfig,
  PositionDirection,
  PostOnlyParams,
  PublicKey,
  SlotSubscriber,
  UserMap,
  UserStatsMap,
  WrappedEvent,
  isVariant,
} from '@drift-labs/sdk';
import {
  convertToNumber,
  calculateBidAskPrice,
  BN,
  QUOTE_PRECISION,
  BASE_PRECISION,
  getOrderSignature,
} from '@drift-labs/sdk';
import { logger } from './common/logger';
import { BBO } from './common/bbo';
import { TransactionResponse, TransactionSignature, VersionedTransactionResponse } from '@solana/web3.js';
import { getErrorCode } from './common/error';
import { isOrderPriceMoreAggressiveThanRef } from './common/ordersUtil';

import { Order } from './common/order';
import { Side } from './common/side';
import { TimeInForce } from './common/timeInForce';
import { SpreadPosition } from './common/spreadPosition';
import { EventParser, BorshCoder } from '@project-serum/anchor';
import { sleep } from './common/utils';
import { BybitClient } from './core/BybitClient';

type Action = {
  baseAssetAmount: BN;
  marketIndex: number;
  direction: PositionDirection;
  price: BN;
  node: DLOBNode;
};

export class JITBot {
  public readonly name: string;
  private exchClient: BybitClient;
  private driftClient: DriftClient;
  private slotSubscriber: SlotSubscriber;
  private _marketInfo: PerpMarketConfig;
  private _publicKey: PublicKey;
  private MAX_POSITION_EXPOSURE = 0.1;
  private MAX_TRADE_SIZE_QUOTE = 100;
  private cexBbo: BBO = new BBO();
  private dexBbo: BBO = new BBO();
  private _started = false;
  private orderLastSeenBaseAmount: Map<string, BN> = new Map(); // need some way to trim this down over time

  private dlob: DLOB;

  private userMap: UserMap;
  private userStatsMap: UserStatsMap;
  private pubKey: PublicKey;
  private exchSymbol: string;
  private spreadPosition: SpreadPosition;
  private inflight = false;

  private nodeMap = new Map<string, DLOBNode>();
  private currentSlot = 0;

  constructor(driftClient: DriftClient, slotSubscriber: SlotSubscriber, marketInfo: PerpMarketConfig) {
    this.name = 'JITBot';
    this.driftClient = driftClient;
    this.slotSubscriber = slotSubscriber;
    this._marketInfo = marketInfo;
    this.spreadPosition = new SpreadPosition();
    this.spreadPosition.minDelta = 1;
  }

  public setExchClient(exchClient: BybitClient) {
    this.exchClient = exchClient;
  }
  public setExchSymbol(symbol: string) {
    this.exchSymbol = symbol;
  }

  public async init() {
    this._publicKey = await this.driftClient.getUserAccountPublicKey();

    this.userMap = new UserMap(this.driftClient, this.driftClient.userAccountSubscriptionConfig);
    this.userStatsMap = new UserStatsMap(this.driftClient, this.driftClient.userAccountSubscriptionConfig);
    await this.userMap.fetchAllUsers();
    await this.userStatsMap.fetchAllUserStats();

    this.dlob = new DLOB();

    await this.dlob.initFromUserMap(this.userMap, this.slotSubscriber.getSlot());

    await this.syncDelta();

    this.pubKey = await this.driftClient.getUserAccountPublicKey();
  }

  public async start() {
    logger.info('Starting JITBot');
    this._started = true;
  }

  public async updateDriftBBO() {
    const [bid, ask] = calculateBidAskPrice(
      this.driftClient.getPerpMarketAccount(this._marketInfo.marketIndex).amm,
      this.driftClient.getOracleDataForPerpMarket(this._marketInfo.marketIndex)
    );

    const formattedBidPrice = convertToNumber(bid, PRICE_PRECISION);
    const formattedAskPrice = convertToNumber(ask, PRICE_PRECISION);

    this.dexBbo.setBid(formattedBidPrice);
    this.dexBbo.setAsk(formattedAskPrice);

    this.dlob = new DLOB();
    await this.dlob.initFromUserMap(this.userMap, this.slotSubscriber.getSlot());

    const bestBid = this.dlob.getBestBid(
      this._marketInfo.marketIndex,
      bid,
      this.slotSubscriber.getSlot(),
      MarketType.PERP,
      this.driftClient.getOracleDataForPerpMarket(this._marketInfo.marketIndex)
    );
    const bestAsk = this.dlob.getBestAsk(
      this._marketInfo.marketIndex,
      ask,
      this.slotSubscriber.getSlot(),
      MarketType.PERP,
      this.driftClient.getOracleDataForPerpMarket(this._marketInfo.marketIndex)
    );
    const formattedDLOBBidPrice = convertToNumber(bestBid, PRICE_PRECISION);

    const formattedDLOBAskPrice = convertToNumber(bestAsk, PRICE_PRECISION);
    logger.info(
      `Dlob ${formattedDLOBBidPrice} || ${formattedDLOBAskPrice} | Amm  ${formattedBidPrice} || ${formattedAskPrice} | Cex: ${this.cexBbo.bestBidPrice} | ${this.cexBbo.bestAskPrice}`
    );
  }

  public nodeCanBeFilled(node: DLOBNode, userAccountPublicKey: PublicKey): boolean {
    if (node.haveFilled) {
      logger.error(`already made the JIT auction for ${node.userAccount} - ${node.order.orderId.toString()}`);
      return false;
    }

    if (node.userAccount.equals(userAccountPublicKey)) {
      return false;
    }

    const orderSignature = getOrderSignature(node.order.orderId, node.userAccount);
    const lastBaseAmountFilledSeen = this.orderLastSeenBaseAmount.get(orderSignature);
    if (lastBaseAmountFilledSeen?.eq(node.order.baseAssetAmountFilled)) {
      return false;
    }

    return true;
  }

  private randomIntFromInterval(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  private determineJitAuctionBaseFillAmount(orderBaseAmountAvailable: BN, orderPrice: BN): BN {
    const priceNumber = convertToNumber(orderPrice, PRICE_PRECISION);
    const worstCaseQuoteSpend = orderBaseAmountAvailable
      .mul(orderPrice)
      .div(BASE_PRECISION.mul(PRICE_PRECISION))
      .mul(QUOTE_PRECISION);

    const minOrderQuote = 20;
    let orderQuote = minOrderQuote;
    let maxOrderQuote = convertToNumber(worstCaseQuoteSpend, QUOTE_PRECISION);

    if (maxOrderQuote > this.MAX_TRADE_SIZE_QUOTE) {
      maxOrderQuote = this.MAX_TRADE_SIZE_QUOTE;
    }

    if (maxOrderQuote >= minOrderQuote) {
      orderQuote = this.randomIntFromInterval(minOrderQuote, maxOrderQuote);
    }

    const baseFillAmountNumber = orderQuote / priceNumber;
    let baseFillAmountBN = new BN(baseFillAmountNumber * BASE_PRECISION.toNumber());
    if (baseFillAmountBN.gt(orderBaseAmountAvailable)) {
      baseFillAmountBN = orderBaseAmountAvailable;
    }
    logger.info(
      `jitMaker will fill base amount: ${convertToNumber(
        baseFillAmountBN,
        BASE_PRECISION
      ).toString()} of remaining order ${convertToNumber(orderBaseAmountAvailable, BASE_PRECISION)}.`
    );

    return baseFillAmountBN;
  }

  private getMakerPrice(startPrice: BN, endPrice: BN, slotElapsed: number, aucDuration: number): BN {
    const remaining = new BN(aucDuration - slotElapsed);
    const endPriceMultiplier = endPrice.mul(new BN(slotElapsed));
    const nom = startPrice.mul(remaining).add(endPriceMultiplier);
    return nom.div(new BN(aucDuration));
  }

  public async isTxFilled(txSig: string): Promise<boolean> {
    let tx: TransactionResponse | null = null;
    let attempt = 0;

    while (tx === null && attempt < 10) {
      logger.info('attempting to get tx');
      tx = await this.driftClient.provider.connection.getTransaction(txSig, {
        commitment: 'confirmed',
      });
      attempt += 1;
      await sleep(50);
    }
    const eventParser = new EventParser(
      this.driftClient.program.programId,
      new BorshCoder(this.driftClient.program.idl)
    );
    let isFilled = false;
    eventParser.parseLogs(tx.meta.logMessages, (data) => {
      if (data.name != 'OrderActionRecord') {
        return;
      }
      if (data.data.fillRecordId !== null) {
        console.log(data);
        isFilled = true;
      }
    });
    return isFilled;
  }

  private async handleAuctionNode(slot: number) {
    if (this.nodeMap.size == 0) {
      return;
    }
    this.nodeMap.forEach(async (node, nodeId) => {
      await this.tryJIT(slot, node);
    });
  }

  private async tryJIT(slot: number, node: DLOBNode) {
    const oracleData = this.driftClient.getOracleDataForPerpMarket(this._marketInfo.marketIndex);

    this.orderLastSeenBaseAmount.set(
      getOrderSignature(node.order.orderId, node.userAccount),
      node.order.baseAssetAmountFilled
    );

    const oraclePrice = oracleData.price;
    // calculate jit maker order params
    const orderMarketIdx = node.order.marketIndex;
    const orderDirection = node.order.direction;
    const isOrderLong = isVariant(orderDirection, 'long');
    const jitMakerDirection = isOrderLong ? PositionDirection.SHORT : PositionDirection.LONG;
    const isMakerDirectionLong = !isOrderLong;

    const auctionStartOffset = node.order.auctionStartPrice;
    const auctionEndOffset = node.order.auctionEndPrice;

    const auctionStartPrice = isOrderLong ? oraclePrice.sub(auctionStartOffset) : oraclePrice.add(auctionStartOffset);
    const auctionEndPrice = isOrderLong ? oraclePrice.add(auctionEndOffset) : oraclePrice.sub(auctionEndOffset);

    const orderSlot = node.order.slot.toNumber();
    const aucDur = node.order.auctionDuration;
    const aucEnd = orderSlot + aucDur;
    const slotElapsed = slot - orderSlot;
    const slotRemaining = aucEnd - slot;

    logger.info(
      `${
        this.name
      } quoting order for node: ${node.userAccount.toBase58()} - ${node.order.orderId.toString()}, orderBaseFilled: ${convertToNumber(
        node.order.baseAssetAmountFilled,
        BASE_PRECISION
      )}/${convertToNumber(
        node.order.baseAssetAmount,
        BASE_PRECISION
      )}  newAuctionStartPrice: ${auctionStartPrice} | newAuctionEndPrice: ${auctionEndPrice} | slotRemaining: ${slotRemaining}`
    );

    const jitMakerPrice = this.getMakerPrice(auctionStartPrice, auctionEndPrice, slotElapsed, aucDur);

    const convertedMakerPrice = convertToNumber(jitMakerPrice, PRICE_PRECISION);
    const refPrice = jitMakerDirection == PositionDirection.LONG ? this.cexBbo.bestAskPrice : this.cexBbo.bestBidPrice;

    const jitMakerBaseAssetAmount = this.determineJitAuctionBaseFillAmount(
      node.order.baseAssetAmount.sub(node.order.baseAssetAmountFilled),
      jitMakerPrice
    );
    if (slotRemaining < 0) {
      this.onTxFailedRemoveNode(node);
      logger.info(`removed node nodeId : ${node.order.orderId.toString()} slotRemaining: ${slotRemaining}`);
      return;
    }

    try {
      if (isOrderPriceMoreAggressiveThanRef(convertedMakerPrice, refPrice, isMakerDirectionLong)) {
        logger.info(
          `Insufficient Edge convertMakerPrice: ${convertedMakerPrice} refPrice: ${refPrice} isLong: ${isMakerDirectionLong}`
        );
        return;
      }
      logger.info(
        `oraclePrice: ${oraclePrice} auctionStartOffset: ${auctionStartOffset} auction start price: ${auctionStartPrice},endOffset: ${auctionEndOffset} auction end price: ${auctionEndPrice}, ref price: ${refPrice}
                ${this.name} propose to fill jit auction on market ${orderMarketIdx}: ${JSON.stringify(
          orderDirection
        )}: ${convertToNumber(jitMakerBaseAssetAmount, BASE_PRECISION).toFixed(4)}, limit price: ${convertToNumber(
          jitMakerPrice,
          PRICE_PRECISION
        ).toFixed(4)}, it has been ${slotElapsed} slots since order, auction ends in ${slotRemaining} slots`
      );

      const txSig = await this.executeAction({
        baseAssetAmount: jitMakerBaseAssetAmount,
        marketIndex: node.order.marketIndex,
        direction: jitMakerDirection,
        price: jitMakerPrice,
        node: node,
      });
      const isTxFilled = await this.isTxFilled(txSig);

      if (!isTxFilled) {
        logger.info(`${this.name} jit auction not filled, txSig: ${txSig}}`);
        return;
      }
      logger.info(
        `${
          this.name
        }: JIT auction filled (account: ${node.userAccount.toString()} - ${node.order.orderId.toString()}) ${txSig}`
      );
      return txSig;
    } catch (error) {
      this.onTxFailedRemoveNode(node);
      node.haveFilled = false;

      const errorCode = getErrorCode(error);

      logger.error(
        `Error (${errorCode}) ${error} filling JIT auction (account: ${node.userAccount.toString()} - ${node.order.orderId.toString()})`
      );
    }

    return null;
  }

  public onEventUpdate(event: WrappedEvent<keyof EventMap>) {
    if (!this._started) {
      return;
    }
    if (event.eventType != 'OrderActionRecord') {
      return;
    }
    // @ts-ignore
    if (event.marketIndex != this._marketInfo.marketIndex) {
      return;
    }
    //@ts-ignore
    if (event.maker == this._publicKey.toBase58()) {
      this.hedgeOwnFill(event);
      logger.info(`Hedged Fill! ${JSON.stringify(event)}`);
      return;
    }
  }

  public async onSlotUpdate(slot: number) {
    this.currentSlot = slot;
    if (!this._started) {
      return;
    }
    this.updateDriftBBO();
    this.dlob.initFromUserMap(this.userMap, slot);
    const nodesToFill = await this.dlob.findJitAuctionNodesToFill(
      this._marketInfo.marketIndex,
      slot,
      this.driftClient.getOracleDataForPerpMarket(this._marketInfo.marketIndex),
      MarketType.PERP
    );
    nodesToFill.forEach(async (node) => {
      await this.onNodeUpdate(node);
    });

    logger.info(`JIT BOT: slot update: ${slot}`);
  }
  public async onPosition(data: any) {
    logger.info(JSON.stringify(data));
  }
  public async onTrade(data: any) {
    logger.info(JSON.stringify(data));
  }
  public async onFill(data: Map<string, any>) {
    this.inflight = false;
    logger.info(JSON.stringify(data));
  }
  public async onBestQuote(data: Map<string, any>) {
    this.cexBbo.onUpdate(data);
  }

  public async onOrderBook(data: Map<string, any>) {
    logger.info('JIT BOT: ' + data);
  }

  private async onNodeUpdate(nodeToFill: NodeToFill) {
    if (!this.nodeCanBeFilled(nodeToFill.node, this.pubKey)) {
      return;
    }
    if (this.inflight) {
      // skip processing if algo sending hedging order
      return;
    }
    if (!this.cexBbo.isReady()) {
      return;
    }
    this.nodeMap.set(nodeToFill.node.order.orderId.toString(), nodeToFill.node);
    await this.handleAuctionNode(this.slotSubscriber.getSlot());
  }

  private onTxFailedRemoveNode(node: DLOBNode) {
    this.nodeMap.delete(node.order.orderId.toString());
    logger.info(`removed node nodeId : ${node.order.orderId.toString()} tx failed`);
  }

  private async syncDelta(): Promise<void> {
    for await (const p of this.driftClient.getUserAccount().perpPositions) {
      if (p.baseAssetAmount.isZero()) {
        continue;
      }
      if (p.marketIndex == this._marketInfo.marketIndex) {
        this.spreadPosition.dexDelta = convertToNumber(p.baseAssetAmount, BASE_PRECISION);
      }
    }

    const positionsResp = await this.exchClient.getPositions();

    positionsResp.result.forEach((p) => {
      const positionData = p.data;
      if (p.is_valid && positionData.size !== 0) {
        if (positionData.symbol == this.exchSymbol) {
          this.spreadPosition.cexDelta = positionData.size;
        }
      }
    });
    if (!this.spreadPosition.isRoughlyBalanced()) {
      this.forceHedge();
    }

    this.spreadPosition.printDelta();
  }

  private async hedgeOwnFill(event: any) {
    const isTakerLong = event.takerOrderDirection == PositionDirection.LONG;
    const hedgeDirection = isTakerLong ? Side.SELL : Side.BUY;
    const hedgePrice = hedgeDirection == Side.BUY ? this.cexBbo.bestAskPrice : this.cexBbo.bestBidPrice;
    const hedgeQty = convertToNumber(event.takerOrderBaseAssetAmount, BASE_PRECISION);
    const order = new Order(this.exchSymbol, hedgeDirection, hedgeQty, hedgePrice, TimeInForce.ImmediateOrCancel);
    const orderResp = await this.exchClient.sendOrder(order);
    logger.info(`Hedge own fill order response: ${JSON.stringify(orderResp)}`);
  }

  private async forceHedge() {
    this.inflight = true;
    const deltaToHedge = this.spreadPosition.getNetDelta();
    const isLong = deltaToHedge > 0;

    const offset = 2 / 1e4;
    const takerPrice = isLong ? this.cexBbo.bestBidPrice : this.cexBbo.bestAskPrice;
    const offsetTakerPrice = isLong ? takerPrice * (1 - offset) : takerPrice * (1 + offset);
    const roundedTakerPrice = Math.round(offsetTakerPrice * 1e4) / 1e4;

    const order = new Order(
      this.exchSymbol,
      isLong ? Side.SELL : Side.BUY,
      Math.abs(deltaToHedge),
      roundedTakerPrice,
      TimeInForce.ImmediateOrCancel
    );

    this.exchClient.sendOrder(order);
    logger.info(`Force hedge order response: ${JSON.stringify(order)}`);
  }

  private async executeAction(action: Action): Promise<TransactionSignature> {
    const takerUserAccount = (await this.userMap.mustGet(action.node.userAccount.toString())).getUserAccount();
    const takerAuthority = takerUserAccount.authority;

    const takerUserStats = await this.userStatsMap.mustGet(takerAuthority.toString());
    const takerUserStatsPublicKey = takerUserStats.userStatsAccountPublicKey;
    return await this.driftClient.placeAndMakePerpOrder(
      {
        orderType: OrderType.LIMIT,
        marketIndex: action.marketIndex,
        baseAssetAmount: action.baseAssetAmount,
        direction: action.direction,
        price: action.price,
        postOnly: PostOnlyParams.TRY_POST_ONLY,
        immediateOrCancel: true,
      },
      {
        taker: action.node.userAccount,
        order: action.node.order,
        takerStats: takerUserStatsPublicKey,
        takerUserAccount: takerUserAccount,
      }
    );
  }
}
